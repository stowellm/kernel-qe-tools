import unittest
from unittest import mock

from cki_lib import misc
from freezegun import freeze_time

from kernel_qe_tools.ci_tools import result2osci


class TestResult2osci(unittest.TestCase):
    """Test pusblish result to OSCI."""

    @mock.patch('kernel_qe_tools.ci_tools.result2osci.get_brew_brew_build')
    @mock.patch('kernel_qe_tools.ci_tools.result2osci.stomp.StompClient')
    def test_result2osci_main_valid(self, mock_stomp, mock_brewbuild):
        """Smoke test with a valid parameters."""
        mock_brewbuild.return_values = {}
        mock_stomp.return_values = True
        with misc.tempfile_from_string(b'') as certfile:
            params = ["--source-nvr", "test-1234-45.el9",
                      "--log-url", "https://test.com/log",
                      "--run-url", "https://test.com/run",
                      "--pipeline-id", "123456",
                      "--certificate", f"{certfile}"]

            cases = (
                ("complete", "passed"),
                ("complete", "failed"),
                ("complete", "info"),
                ("error", None),
            )
            for (status, result) in cases:
                params.extend(["--test-status", status])
                if result:
                    params.extend(["--test-result", result])
                result = result2osci.main(params)
                self.assertEqual(result, 0)

    @freeze_time("2024-11-11 13:20:26.610365")
    @mock.patch('kernel_qe_tools.ci_tools.result2osci.get_brew_brew_build')
    def test_result2osci_create(self, mock_brewbuild):
        """test with create function."""
        self.maxDiff = None
        # Common info
        buildname = 'testbuild'
        taskid = '123456'
        owner_name = 'testowner'
        owner_name_upstream = 'testownerupstream'
        source = 'testsouce'
        nvr = 'test-1234-45.el9'
        pipelineid = '1234'

        run_info = {'log': 'test_run_log', 'url': 'test_run_url'}
        test_info = {'category': 'test_category', 'namespace': 'test_namespace',
                     'type': 'test_type', 'result': 'passed'}

        contact = {'docs': 'https://docs.engineering.redhat.com/display/KQ/CI',
                   'email': 'kernel-ci-list@redhat.com', 'name': 'kernel-qe-ci',
                   'slack': 'https://redhat.enterprise.slack.com/archives/C04N172J4MU',
                   'team': 'kernel-qe',
                   'url': 'https://redhat.enterprise.slack.com/archives/C04N172J4MU'}
        pipeline = {'id': pipelineid, 'name': 'kernel-qe-ci-gating'}
        run = {'log': 'test_run_log', 'url': 'test_run_url'}
        encoded_xunit = ('H4sIAJoEMmcC/02PSw7CMAxE9z2FlQMQ2KfthhOwYVlZyNBK+Uh2jMrtSWnasrKeZjweu34O'
                         'Ht7EMqXYmsvpbKDvGpdJsuhURtcAHAgRA7VGNATkj1nEKj9QNnVBJlGfpTqKx6eXbLBidV8x'
                         '4x2ZxqQl4Vb3YGR6rlEDaxyUvbF7mD3SnN2u/5ravWr5wv6/8QVB7e0A6wAAAA==')
        test = {'category': 'test_category',
                'docs': 'https://docs.engineering.redhat.com/display/KQ/CI',
                'namespace': 'test_namespace', 'type': 'test_type',
                'xunit': encoded_xunit,
                'result': 'passed'}

        result_without_artifact = {'contact': contact,
                                   'generated_at': '2024-11-11T13:20:26.610365Z',
                                   'pipeline': pipeline, 'run': run, 'system': [],
                                   'test': test, 'version': '1.1.14'}

        # Generate return values for our mock
        buildinfo = {'name': buildname, 'task_id': taskid,
                     'owner_name': owner_name, 'source': source}
        extra = {'extra': {
            'custom_user_metadata': {'osci': {'upstream_owner_name': owner_name_upstream}}}
        }

        mock_brewbuild.side_effect = [buildinfo, buildinfo | extra]

        # Test cases definition
        artifact_with_owner_name = {
            'artifact': {
                'component': buildname, 'id': taskid, 'issuer': owner_name,
                'source': source, 'nvr': nvr, 'scratch': False, 'type': 'brew-build'
            }
        }

        artifact_with_owner_name_upstream = {
            'artifact': {
                'component': buildname, 'id': taskid,
                'issuer': owner_name_upstream, 'source': source,
                'nvr': nvr, 'scratch': False, 'type': 'brew-build'
            }
        }

        cases = (
            ('issuer info from the brew build owner', artifact_with_owner_name),
            ('issuer info from the upstream build owner', artifact_with_owner_name_upstream),
        )

        for (description, artifact) in cases:
            with self.subTest(description):
                result = result2osci.create(nvr, run_info, test_info, pipelineid, 'complete')
                self.assertEqual(result, result_without_artifact | artifact)
