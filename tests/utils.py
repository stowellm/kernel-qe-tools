"""Utils module."""
import contextlib
import os
import pathlib
import shutil
import tempfile


@contextlib.contextmanager
def run_in_a_sanbox_directory():
    """Create a sanbox directory."""
    tmpdir = tempfile.mkdtemp()
    origin = pathlib.Path().absolute()
    try:
        os.chdir(tmpdir)
        yield tmpdir
    finally:
        os.chdir(origin)
        shutil.rmtree(tmpdir)
