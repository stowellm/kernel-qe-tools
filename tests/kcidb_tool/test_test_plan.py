"""Test Parser."""

from importlib.resources import files
import json
import pathlib
import tempfile
import unittest

from kernel_qe_tools.kcidb_tool.dataclasses import NVR
from kernel_qe_tools.kcidb_tool.dataclasses import TestPlanParserArguments
from kernel_qe_tools.kcidb_tool.test_plan_parser import TestPlanParser

ASSETS = files(__package__) / 'assets'


class TestTestPlanParser(unittest.TestCase):
    """Test Plan Parser."""

    maxDiff = None

    def setUp(self):
        """Initialize tester."""
        self.args = TestPlanParserArguments(
            arch='x86_64',
            brew_task_id='47919042',
            builds_origin='b_origin',
            checkout='checkout_id',
            checkout_origin='c_origin',
            nvr=NVR('kernel_rt', '5.14.0', '162.2.1.164.el9'),
            src_nvr=NVR('kernel', '5.14.0', '162.2.1.164.el9'),
        )
        self.parser = TestPlanParser(self.args)

    def test_add_build(self):
        """Check add_build function."""
        expected = {
            'id': 'b_origin:checkout_id_x86_64_kernel_rt',
            'origin': 'b_origin',
            'checkout_id': 'c_origin:checkout_id',
            'architecture': 'x86_64',
            'valid': True,
            'misc': {
                'test_plan_missing': True,
                'package_name': 'kernel_rt',
                'package_release': '162.2.1.164.el9',
                'package_version': '5.14.0',
                'provenance': [
                    {
                        'function': 'executor',
                        # pylint: disable=line-too-long
                        'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042',  # noqa: E501
                        'service_name': 'buildsystem'
                    }
                ]
            }
        }

        self.parser.add_checkout()
        # Before we don't have any build
        self.assertEqual(0, len(self.parser.report['builds']))

        # We insert one
        self.parser.add_build()
        self.assertEqual(1, len(self.parser.report['builds']))

        self.assertDictEqual(expected, self.parser.report['builds'][0])

        # If we add twice the same build_id, we only store one
        self.parser.add_build()
        self.assertEqual(1, len(self.parser.report['builds']))

    def test_add_checkout(self):
        """Check add_checkout function."""
        expected_checkout = {
            'id': 'c_origin:checkout_id',
            'origin': 'c_origin',
            'valid': True,
            'misc': {
                'is_public': False,
                'kernel_version': '5.14.0-162.2.1.164.el9',
                'provenance': [
                    {
                        'function': 'executor',
                        'service_name': 'buildsystem',
                        # pylint: disable=line-too-long
                        'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042'  # noqa: E501
                    }
                ],
                'source_package_name': 'kernel',
                'source_package_release': '162.2.1.164.el9',
                'source_package_version': '5.14.0'
            }
        }
        # Empty checkout
        self.assertIsNone(self.parser.report['checkout'])

        # Adding the checkout
        self.parser.add_checkout()

        self.assertDictEqual(expected_checkout, self.parser.report['checkout'])

    def test_process(self):
        """Check if we can create the test plan."""
        self.parser.process()
        self.assertIsNotNone(self.parser.report['checkout'])
        self.assertEqual(1, len(self.parser.report['builds']))
        self.assertEqual(0, len(self.parser.report['tests']))

    def test_write_test_plan(self):
        """Check write the final result."""
        output_file = 'kcidb.json'
        expected_kcidb_file = pathlib.Path(ASSETS, 'kcidb_test_plan.json')
        self.parser.process()
        fake_file = pathlib.Path('/tmp/', 'kcidb_test_plan.json')
        self.parser.write(fake_file)
        with tempfile.TemporaryDirectory() as tmpdir:
            kcidb_file = pathlib.Path(tmpdir, output_file)
            self.parser.write(kcidb_file)

            computed = json.loads(kcidb_file.read_text(encoding='utf-8'))
            expected = json.loads(expected_kcidb_file.read_text(encoding='utf-8'))

        for obj_type, expected_values in expected.items():
            # Skip version checking
            if obj_type == "version":
                continue

            self.assertEqual(expected_values, computed[obj_type])
