"""Test Clean Dict."""

from importlib.resources import files
import pathlib
import unittest
from unittest import mock
import xml.etree.ElementTree as ET

from freezegun import freeze_time

from kernel_qe_tools.kcidb_tool.bkr import utils

ASSETS = files(__package__) / 'assets'


class TestBeakerUtils(unittest.TestCase):
    """Test Parser."""

    def test_get_second(self):
        """Check get_second function."""
        time_str = '01:00:44'
        seconds = 3644
        self.assertEqual(seconds, utils.get_seconds(time_str))

        time_str = '00:00:00'
        seconds = 0
        self.assertEqual(seconds, utils.get_seconds(time_str))

    def test_get_duration(self):
        """Ensure get_duration works."""
        cases = (
            ('Test with 0 seconds', '00:00:00', 0),
            ('Test with less than one day', '10:00:44', 36044),
            ('Test with days', '2 days, 0:30:33', 174633),
            ('Test with weeks', '1 week, 0:30:33', 606633),
            ('Test with days and weeks', '1 week, 2 days, 10:00:21', 813621),
        )

        for (description, bkr_duration, expected) in cases:
            with self.subTest(description):
                self.assertEqual(expected, utils.get_duration(bkr_duration))

    def test_get_utc_datetime(self):
        """Check get_utc_datetime function."""
        datetime_str = '2022-04-09 00:37:39'
        expected_utc_time = '2022-04-09T00:37:39+00:00'

        self.assertEqual(expected_utc_time, utils.get_utc_datetime(datetime_str))

        # When we don't have any datetime in beaker, we send None
        mocked_utc_time = '2000-01-01T00:00:00'
        expected_utc_time = f'{mocked_utc_time}+00:00'
        with freeze_time(mocked_utc_time):
            self.assertEqual(expected_utc_time, utils.get_utc_datetime(None))

    def test_get_int(self):
        """Check get_int function."""
        text_to_int = '123'
        self.assertEqual(123, utils.get_int(text_to_int))

        # Invalid cases
        text_to_int = 'abc'
        self.assertIsNone(utils.get_int(text_to_int))
        text_to_int = None
        self.assertIsNone(utils.get_int(text_to_int))

    def test_get_console_log(self):
        """Test get_console_log_url."""
        recipe_id = '12557513'
        expected = 'https://beaker.engineering.redhat.com/recipes/12557513/logs/console.log'
        self.assertEqual(utils.get_console_log_url(recipe_id), expected)

    def test_get_recipe_url(self):
        """Test get_recipe_url."""
        recipe_id = '12557513'
        expected = 'https://beaker.engineering.redhat.com/recipes/12557513'
        self.assertEqual(utils.get_recipe_url(recipe_id), expected)

    @mock.patch('kernel_qe_tools.kcidb_tool.bkr.utils.sanitize_kcidb_status')
    def test_get_test_status(self, mock_sanitize):
        """Test test status."""
        bkr_content = pathlib.Path(ASSETS, 'beaker_with_miss.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        tasks = root.findall('.//task')

        cases = (
            ('Task without External Watchdog', tasks[0], False),
            ('Task reaching the External Watchdog', tasks[1], False),
            ('Task without being executed with External Watchdog', tasks[2], True),
            ('A task with status Cancelled', tasks[3], True)
        )
        for description, task, is_task_missed in cases:
            with self.subTest(description):
                mock_sanitize.reset_mock()
                status = utils.sanitize_test_status(task)
                if is_task_missed:
                    self.assertEqual('MISS', status)
                    mock_sanitize.assert_not_called()
                else:
                    mock_sanitize.assert_called()

    def test_get_task_maintainers(self):
        """Ensure get_task_maintainers works."""
        bkr_content = \
            pathlib.Path(ASSETS, 'beaker_task_maintainers.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        tasks = root.findall('.//task')

        user_one = {'email': 'user_1@redhat.com', 'gitlab': 'user_1', 'name': 'User 1'}
        user_two = {'email': 'user_2@redhat.com', 'name': 'User 2'}
        cases = (
            ('A task without parameter', tasks[0], []),
            ('A task with parameter but with maintainer', tasks[1], []),
            ('A task with a maintainer', tasks[2], [user_one]),
            ('A task with maintainers', tasks[3], [user_one, user_two]),
            ('A task with a maintainer without email', tasks[4], []),
        )
        for description, task, expected in cases:
            with self.subTest(description):
                self.assertListEqual(expected, utils.get_tasks_maintainers(task))

    def test_get_results(self):
        """Check get_results function."""
        xml_content_without_results = "<xml><results></results></xml>"

        xml_content_with_results = """
        <xml>
          <results>
            <result path="result_one" result="Pass">
              <logs>
                <log name="bar.log" href="http://srv/bar.log"/>
              </logs>
            </result>
            <result path="result_two" result="Fail">
              <logs>
                <log name="bar.log" href="http://srv/bar.log"/>
                <log name="foo.log" href="http://srv/foo.log"/>
              </logs>
            </result>
          </results>
        </xml>
        """

        expected_with_results = [
            {'id': 'test_id.1',
             'comment': 'result_one',
             'name': 'result_one',
             'status': 'PASS',
             'output_files': [
                 {'name': 'bar.log', 'url': 'http://srv/bar.log'}
                ]
             },
            {'id': 'test_id.2',
             'comment': 'result_two',
             'name': 'result_two',
             'status': 'FAIL',
             'output_files': [
                 {'name': 'bar.log', 'url': 'http://srv/bar.log'},
                 {'name': 'foo.log', 'url': 'http://srv/foo.log'}
                ]
             }
        ]

        xml_content_with_bkr_info_in_text = """
        <xml>
          <results>
            <result path="/" result="Warn">External Watchdog Expired</result>
          </results>
        </xml>
        """
        expected_with_bkr_info_in_text = [
            {'id': 'test_id.1',
             'comment': 'External Watchdog Expired',
             'name': 'External Watchdog Expired',
             'status': 'ERROR',
             },
        ]

        xml_content_with_kernel_panic = """
        <xml>
          <results>
            <result path="/" result="Panic">Kernel Panic</result>
          </results>
        </xml>
        """
        expected_with_kernel_panic = [
            {'id': 'test_id.1',
             'comment': 'Kernel Panic',
             'name': 'Kernel Panic',
             'status': 'FAIL',
             },
        ]

        xml_content_with_an_empty_path = """
        <xml>
         <results>
           <result path="" result="Pass">
           </result>
         </results>
        </xml>
        """

        expected_with_an_empty_path = [
            {'id': 'test_id.1',
             'comment': 'UNDEFINED',
             'name': 'UNDEFINED',
             'status': 'PASS',
             },
        ]

        expected_miss_status = [
            {'id': 'test_id.1',
             'comment': 'External Watchdog Expired',
             'name': 'External Watchdog Expired',
             'status': 'MISS',
             },
        ]

        test_without_miss = {
            'id': 'test_id',
            'status': 'SOME_STATUS'
        }

        test_with_miss = {
            'id': 'test_id',
            'status': 'MISS'
        }

        cases = (
            ('Without results', xml_content_without_results, [], test_without_miss),
            ('With results', xml_content_with_results, expected_with_results, test_without_miss),
            ('Result with Beaker info in text',
             xml_content_with_bkr_info_in_text, expected_with_bkr_info_in_text, test_without_miss),
            ('Result with a Kernel Panic',
             xml_content_with_kernel_panic, expected_with_kernel_panic, test_without_miss),
            ('A result with a empty path',
             xml_content_with_an_empty_path, expected_with_an_empty_path, test_without_miss
             ),
            ('Test status is MISS',
             xml_content_with_bkr_info_in_text, expected_miss_status, test_with_miss),
        )

        for description, xml_content, expected_results, test in cases:
            with self.subTest(description):
                results = ET.fromstring(xml_content).find('results')
                self.assertListEqual(expected_results, utils.get_results(test, results))

    def test_get_duration_between_dates(self):
        """Ensure get_dutarion_between_dates works."""
        cases = (
            ('Less than one day', '2024-07-09 01:45:00', '2024-07-09 06:45:30', '5:00:30'),
            ('More than one day', '2024-07-09 01:45:00', '2024-07-19 06:45:30', '10 days, 5:00:30'),
        )
        for description, start, end, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, utils.get_duration_between_dates(start, end))

    def test_create_system_provision_task_when_everything_is_ok(self):
        """Test when everything is ok."""
        bkr_content = pathlib.Path(ASSETS,
                                   'beaker_system_provision.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        recipe = root.findall('.//recipe')[0]

        task = utils.create_system_provision_task(recipe, 1)

        results = task.findall('.//result')

        self.assertEqual('system_provision', task.get('name'))
        self.assertEqual('1', task.get('id'))
        self.assertEqual('2024-07-08 01:13:32', task.get('start_time'))
        self.assertEqual('0:05:02', task.get('duration'))
        self.assertEqual(5, len(task.findall('./logs/log')))
        self.assertEqual(1, len(results))
        self.assertEqual('Setup OK', results[0].get('path'))
        self.assertEqual(5, len(results[0].findall('.//log')))
        self.assertEqual('PASS', task.get('result'))
        self.assertEqual('True', task.get('fake'))

    def test_create_system_provision_task_when_the_recipe_is_cancelled_before_running(self):
        """Test when the recipe is cancelled before running."""
        bkr_content = pathlib.Path(ASSETS,
                                   'beaker_system_provision.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        recipe = root.findall('.//recipe')[1]

        task = utils.create_system_provision_task(recipe, 1)

        results = task.findall('.//result')

        self.assertEqual('system_provision', task.get('name'))
        self.assertEqual('1', task.get('id'))
        self.assertIsNone(task.get('start_time'))
        self.assertIsNone(task.get('duration'))
        self.assertEqual(0, len(task.findall('./logs/log')))
        self.assertEqual(1, len(results))
        self.assertEqual('Cancelled before running', results[0].get('path'))
        self.assertEqual(0, len(results[0].findall('.//log')))
        self.assertEqual("ERROR", task.get("result"))
        self.assertEqual('True', task.get('fake'))

    def test_create_system_provision_task_when_the_installation_failed(self):
        """Test when the installation for a recipe failed."""
        bkr_content = pathlib.Path(ASSETS,
                                   'beaker_system_provision.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        recipe = root.findall('.//recipe')[2]

        task = utils.create_system_provision_task(recipe, 1)

        results = task.findall('.//result')

        self.assertEqual('system_provision', task.get('name'))
        self.assertEqual('1', task.get('id'))
        self.assertEqual('2024-07-11 14:00:43', task.get('start_time'))
        self.assertEqual('0:13:02', task.get('duration'))
        self.assertEqual(5, len(task.findall('./logs/log')))
        self.assertEqual(1, len(results))
        self.assertEqual('Installation failed', results[0].get('path'))
        self.assertEqual(5, len(results[0].findall('.//log')))
        self.assertEqual("ERROR", task.get("result"))
        self.assertEqual('True', task.get('fake'))

    def test_create_system_provision_task_when_the_recipe_is_provisioned_but_all_tasks_missed(self):
        """Test when the recipe is provisioned but all tasks are missed."""
        bkr_content = pathlib.Path(ASSETS,
                                   'beaker_system_provision.xml').read_text(encoding='utf-8')
        root = ET.fromstring(bkr_content)
        recipe = root.findall('.//recipe')[3]

        task = utils.create_system_provision_task(recipe, 1)

        results = task.findall('.//result')

        self.assertEqual('system_provision', task.get('name'))
        self.assertEqual('1', task.get('id'))
        self.assertEqual('2024-07-11 02:56:41', task.get('start_time'))
        self.assertEqual('0:04:48', task.get('duration'))
        self.assertEqual(5, len(task.findall('./logs/log')))
        self.assertEqual(1, len(results))
        self.assertEqual('All tasks missed', results[0].get('path'))
        self.assertEqual(5, len(results[0].findall('.//log')))
        self.assertEqual("ERROR", task.get("result"))
        self.assertEqual('True', task.get('fake'))

    def test_post_process_result_when_the_output_is_the_same(self):
        """Ensure post_process_results works when the output is the same."""
        normal_results = [
            {
                'id': 'test_id.1',
                'comment': 'result_one',
                'name': 'result_one',
                'status': 'PASS',
            }
        ]

        panic_results = [
            {
                'id': 'test_id.1',
                'comment': 'Panic: Kernel panic',
                'name': 'Panic: Kernel panic',
                'status': 'FAIL',
            }
        ]

        cases = (
            ('Normal results', normal_results),
            ('Panic results without External Watchdog', panic_results),
        )

        for description, results in cases:
            with self.subTest(description):
                self.assertListEqual(results, utils.post_process_results(results))

    def test_post_process_result_with_watchdog_and_panic(self):
        """Ensure post_process_results works when the output is the same."""
        panic_result = {
            'id': 'test_id.1',
            'comment': 'Panic: Kernel panic',
            'name': 'Panic: Kernel panic',
            'status': 'FAIL',
        }

        external_watchdog_result = {
            'id': 'test_id.1',
            'comment': 'Warn: External Watchdog Expired',
            'name': 'Warn: External Watchdog Expired',
            'status': 'ERROR',
        }

        normal_result = {
            'id': 'test_id.1',
            'comment': 'result_one',
            'name': 'result_one',
            'status': 'PASS',
        }

        expected = [
            {
                'id': 'test_id.1',
                'comment': 'Panic: Kernel panic',
                'name': 'Panic: Kernel panic',
                'status': 'FAIL',
            },
            {
                'id': 'test_id.2',
                'comment': 'result_one',
                'name': 'result_one',
                'status': 'PASS',
            }
        ]

        omit_external_watchdog_message = ('Omitting External Watchdog Expired result because a '
                                          'Kernel Panic is present')
        fixing_ids_message = 'Some results have been changed, fixing ids'

        cases = (
            ('Panic First', [panic_result, normal_result, external_watchdog_result]),
            ('External Watchdog First', [external_watchdog_result, panic_result, normal_result]),
        )

        for description, results in cases:
            with (
                self.subTest(description),
                self.assertLogs(utils.LOGGER, level="DEBUG") as log_ctx
            ):
                processed_results = utils.post_process_results(results)
                self.assertEqual(len(expected), len(processed_results))
                self.assertListEqual(expected, utils.post_process_results(results))
                self.assertIn(omit_external_watchdog_message, log_ctx.output[0])
                self.assertIn(fixing_ids_message, log_ctx.output[1])
