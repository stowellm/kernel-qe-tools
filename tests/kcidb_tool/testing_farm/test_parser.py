"""Test Parser."""

from importlib.resources import files
import io
import json
import pathlib
import tempfile
import unittest
from unittest import mock
import xml.etree.ElementTree as ET

from freezegun import freeze_time

from kernel_qe_tools.kcidb_tool.dataclasses import ExternalOutputFile
from kernel_qe_tools.kcidb_tool.dataclasses import NVR
from kernel_qe_tools.kcidb_tool.dataclasses import ParserArguments
from kernel_qe_tools.kcidb_tool.testing_farm.parser import TestingFarmParser

ASSETS = files(__package__) / '../assets'


@freeze_time("2024-05-31 13:15:00")
class TestTestingFarmParser(unittest.TestCase):
    """Test Parser."""

    maxDiff = None

    def setUp(self):
        """Initialize tester."""
        self.testing_farm_content = pathlib.Path(ASSETS,
                                                 'testing_farm.xml').read_text(encoding='utf-8')
        self.args = ParserArguments(
            checkout='checkout_id',
            test_plan=False,
            extra_output_files=[],
            brew_task_id='47919042',
            tests_provisioner_url='https://jenkins/job/my_job/1',
            contacts=['John Doe <jdoe@redhat.com>'],
            nvr=NVR('kernel', '5.14.0', '162.2.1.164.el9'),
            src_nvr=NVR('kernel', '5.14.0', '162.2.1.164.el9'),
            builds_origin='b_origin',
            checkout_origin='c_origin',
            tests_origin='t_origin',
            report_rules='[{"when": "always", "send_to": ["ptalbert@redhat.com"]}]',
            submitter='jdoe@redhat.com'
        )
        self.parser = TestingFarmParser(self.testing_farm_content, self.args)

        self.parser.arch = 'x86_64'

        self.parser.test_suite_info = {
            'arch': 'x86_64',
            'start_time': self.parser.start_time,
            'common_logs': [
                {
                    'name': 'console_log',
                    'url': 'http://artifacts.server/testing-farm/10/work_a/console-20.log'
                },
                {
                    'name': 'workdir',
                    'url': 'http://artifacts.server/testing-farm/10/work_a'
                }
            ]
        }

    def test_init(self):
        """Dummy test for init."""
        self.assertEqual('c_origin:checkout_id', self.parser.checkout_id)
        self.assertCountEqual({
            'checkout': None,
            'builds': [],
            'tests': []
        }, self.parser.report)

    def test_build_id(self):
        """Test build id."""
        self.parser.add_build()
        self.assertEqual(self.parser.report['builds'][0]['id'],
                         'b_origin:checkout_id_x86_64_kernel')

    def test_exist_build_by_arch(self):
        """Test exist build by arch function works."""
        self.assertFalse(self.parser.exist_build_by_arch())

        # Add one
        self.parser.add_build()

        # Check again
        self.assertTrue(self.parser.exist_build_by_arch())

    def test_add_build(self):
        """Check add_build function."""
        expected = {
            'id': 'b_origin:checkout_id_x86_64_kernel',
            'origin': 'b_origin',
            'checkout_id': 'c_origin:checkout_id',
            'architecture': 'x86_64',
            'valid': True,
            'misc': {
                'test_plan_missing': False,
                'package_name': 'kernel',
                'package_release': '162.2.1.164.el9',
                'package_version': '5.14.0',
                'provenance': [
                    {
                        'function': 'executor',
                        # pylint: disable=line-too-long
                        'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042',  # noqa: E501
                        'service_name': 'buildsystem'
                    }
                ]
            }
        }

        # Before we don't have any build
        self.assertEqual(0, len(self.parser.report['builds']))

        # We insert one
        self.parser.add_build()
        self.assertEqual(1, len(self.parser.report['builds']))

        self.assertDictEqual(expected, self.parser.report['builds'][0])

        # If we add twice the same build_id, we only store one
        self.parser.add_build()
        self.assertEqual(1, len(self.parser.report['builds']))

    def test_add_checkout(self):
        """Check add_checkout function."""
        expected_checkout = {
            'id': 'c_origin:checkout_id',
            'origin': 'c_origin',
            'tree_name': 'rhel-9.4',
            'start_time': '2024-05-31T13:15:00+00:00',
            'valid': True,
            'misc': {
                'is_public': False,
                'kernel_version': '5.14.0-162.2.1.164.el9',
                'provenance': [
                    {
                        'function': 'executor',
                        'service_name': 'buildsystem',
                        # pylint: disable=line-too-long
                        'url': 'https://brewweb.engineering.redhat.com/brew/taskinfo?taskID=47919042'  # noqa: E501
                    },
                    {
                        'function': 'coordinator',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'report_rules': '[{"when": "always", "send_to": ["ptalbert@redhat.com"]}]',
                'source_package_name': 'kernel',
                'source_package_release': '162.2.1.164.el9',
                'source_package_version': '5.14.0',
                'submitter': 'jdoe@redhat.com'
            }
        }

        # Empty checkout
        self.assertIsNone(self.parser.report['checkout'])

        # Adding the checkout
        self.parser.add_checkout()

        self.assertDictEqual(expected_checkout, self.parser.report['checkout'])

    def test_add_test_only_test_plan(self):
        """Check add_test method when it is a test_plan."""
        beaker_content = """
          <check name="task name"/>
        """
        self.parser.args.test_plan = True
        expected_test = {
            'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_10',
            'origin': 't_origin',
            'build_id': 'b_origin:checkout_id_x86_64_kernel',
            'comment': 'task name',
            'path': 'task_name',
        }
        task = ET.fromstring(beaker_content)

        # Tests empty
        self.assertEqual([], self.parser.report['tests'])

        self.parser.add_test(task, 10)

        self.assertEqual(1, len(self.parser.report['tests']))
        self.assertDictEqual(expected_test, self.parser.report['tests'][0])

    def test_add_test_no_test_plan(self):
        """Check add_test method when it is not a test plan."""
        testing_farm_content = """
        <xml>
          <testcase name="task with results" result="passed" time="38">
            <properties>
              <property name="baseosci.connectable_host" value="some_server"/>
            </properties>
            <logs>
              <log href="https://srv/work_a/console.log" name="console log"/>
              <log href="https://srv/work_a/output.txt" name="testout.log"/>
              <log href="https://srv/work_a" name="workdir"/>
              <log href="https://srv/work_a/result_one/subresult.txt" name="subresult.txt"/>
            </logs>
            <checks>
              <check name="result one" result="pass">
                <logs>
                  <log href="https://srv/work_a/result_one/result_one.txt" name="result_one.txt"/>
                </logs>
              </check>
            </checks>
            <subresults>
              <subresult name="subresult with log" result="pass">
                <logs>
                  <log href="https://srv/work_a/result_one/subresult.txt" name="subresult.txt"/>
                </logs>
              </subresult>
              <subresult name="subresult without log" result="pass"/>
            </subresults>
          </testcase>
          <testcase name="task without results" result="passed" time="10">
            <properties>
              <property name="baseosci.connectable_host" value="some_server"/>
            </properties>
            <logs>
              <log href="https://srv/work_b/console.log" name="console log"/>
              <log href="https://srv/work_b/output.txt" name="testout.log"/>
              <log href="https://srv/work_b" name="workdir"/>
            </logs>
          </testcase>
        </xml>
        """  # noqa: E501
        tasks = ET.fromstring(testing_farm_content).findall('testcase')
        expected_with_results = {
            'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1',
            'log_url': 'https://srv/work_a/output.txt',
            'origin': 't_origin',
            'build_id': 'b_origin:checkout_id_x86_64_kernel',
            'comment': 'task with results',
            'path': 'task_with_results',
            'start_time': '2024-05-31T13:15:00+00:00',
            'duration': 38,
            'output_files': [
                {'name': 'console log', 'url': 'https://srv/work_a/console.log'},
                {'name': 'testout.log', 'url': 'https://srv/work_a/output.txt'},
                {'name': 'workdir', 'url': 'https://srv/work_a'},
                {'name': 'console_log',
                 'url': 'http://artifacts.server/testing-farm/10/work_a/console-20.log'
                 },
                {'name': 'workdir', 'url': 'http://artifacts.server/testing-farm/10/work_a'}
            ],
            'status': 'PASS',
            'misc': {
                'results': [{
                    'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1.1',
                    'comment': 'task_with_results',
                    'name': 'task_with_results',
                    'status': 'PASS',
                    'output_files': [
                        {
                            'name': 'testout.log',
                            'url': 'https://srv/work_a/output.txt'
                        }
                    ]},
                    {
                    'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1.2',
                    'comment': 'result one',
                    'name': 'result one',
                    'status': 'PASS',
                    'output_files': [
                        {
                            'name': 'result_one.txt',
                            'url': 'https://srv/work_a/result_one/result_one.txt'
                        }
                    ]},
                    {
                    'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1.3',
                    'name': 'subresult with log',
                    'comment': 'subresult with log',
                    'status': 'PASS',
                    'output_files': [
                        {
                            'name': 'subresult.txt',
                            'url': 'https://srv/work_a/result_one/subresult.txt'
                        }
                    ]},
                    {
                    'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_1.4',
                    'name': 'subresult without log',
                    'comment': 'subresult without log', 'status': 'PASS'
                    }
                ],
                'provenance': [
                    {
                        'function': 'provisioner',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'maintainers': [
                    {
                        'name': 'John Doe',
                        'email': 'jdoe@redhat.com'
                    }
                ]
            },
            'environment': {
                'comment': 'some_server'
            }
        }
        expected_without_results = {
            'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_2',
            'log_url': 'https://srv/work_b/output.txt',
            'origin': 't_origin',
            'build_id': 'b_origin:checkout_id_x86_64_kernel',
            'comment': 'task without results',
            'path': 'task_without_results',
            'start_time': '2024-05-31T13:15:38+00:00',
            'duration': 10,
            'output_files': [
                {'name': 'console log', 'url': 'https://srv/work_b/console.log'},
                {'name': 'testout.log', 'url': 'https://srv/work_b/output.txt'},
                {'name': 'workdir', 'url': 'https://srv/work_b'},
                {'name': 'console_log',
                 'url': 'http://artifacts.server/testing-farm/10/work_a/console-20.log'
                 },
                {'name': 'workdir', 'url': 'http://artifacts.server/testing-farm/10/work_a'}
            ],
            'status': 'PASS',
            'misc': {
                'results': [{
                    'id': 't_origin:checkout_id_x86_64_kernel_kcidb_tool_2.1',
                    'comment': 'task_without_results',
                    'name': 'task_without_results',
                    'status': 'PASS',
                    'output_files': [
                        {
                            'name': 'testout.log',
                            'url': 'https://srv/work_b/output.txt'
                        }
                    ]},
                ],
                'provenance': [
                    {
                        'function': 'provisioner',
                        'service_name': 'jenkins',
                        'url': 'https://jenkins/job/my_job/1'
                    }
                ],
                'maintainers': [
                    {
                        'name': 'John Doe',
                        'email': 'jdoe@redhat.com'
                    }
                ]
            },
            'environment': {
                'comment': 'some_server'
            }
        }

        # Empty tests
        self.assertEqual([], self.parser.report['tests'])

        # Insert first tests
        self.parser.add_test(tasks[0], 1)

        self.assertEqual(1, len(self.parser.report['tests']))
        self.assertDictEqual(expected_with_results, self.parser.report['tests'][0])

        # Insert second tests
        self.parser.add_test(tasks[1], 2)

        self.assertEqual(2, len(self.parser.report['tests']))
        self.assertDictEqual(expected_without_results, self.parser.report['tests'][1])

    def test_process(self):
        """Check if we can process a testing_farm content."""
        # In the testing_farm.xml we'll obtain 1 build and 5 tests (2 provision)
        self.parser.args.test_plan = True
        self.parser.process()

        self.assertEqual(1, len(self.parser.report['builds']))
        self.assertEqual(5, len(self.parser.report['tests']))

        self.parser.args.test_plan = False
        self.parser.process()

        self.assertEqual(1, len(self.parser.report['builds']))
        self.assertEqual(5, len(self.parser.report['tests']))

    def test_write_no_test_plan(self):
        """Check write the final result."""
        output_file = 'kcidb.json'
        expected_kcidb_file = pathlib.Path(ASSETS, 'kcidb_testing_farm.json')
        self.parser.args.test_plan = False
        self.parser.args.extra_output_files = [
            ExternalOutputFile(name='job_url', url='https://jenkins/job/my_job/1'),
            ExternalOutputFile(name='brew_url', url='https://brew/some_url')
        ]
        self.parser.process()
        fake_file = pathlib.Path('/tmp/', 'kcidb_testing_farm.json')
        self.parser.write(fake_file)
        with tempfile.TemporaryDirectory() as tmpdir:
            kcidb_file = pathlib.Path(tmpdir, output_file)
            self.parser.write(kcidb_file)

            computed = json.loads(kcidb_file.read_text(encoding='utf-8'))
            expected = json.loads(expected_kcidb_file.read_text(encoding='utf-8'))

        for obj_type, expected_values in expected.items():
            # Skip version checking
            if obj_type == "version":
                continue

            self.assertEqual(expected_values, computed[obj_type])

    def test_get_tree_name(self):
        """Test to get the tree name using the distro field."""
        right_tree_name = """
        <xml>
          <testsuite>
            <testing-environment name="provisioned">
              <property name="compose" value="RHEL-9.4.0-Nightly"/>
            </testing-environment>
          </testsuite>
        </xml>
        """
        without_tree_name = """
        <xml>
          <testsuite/>
        </xml>
        """
        invalid_tree_name = """
        <xml>
          <testsuite>
            <testing-environment name="provisioned">
              <property name="compose" value="RHEL-9-Nightly"/>
            </testing-environment>
          </testsuite>
        </xml>
        """

        cases = (
            ('Right tree name', right_tree_name, 'rhel-9.4'),
            ('Without tree name', without_tree_name, None),
            ('Invalid tree name', invalid_tree_name, None),
        )

        for description, testing_farm_content, expected in cases:
            with self.subTest(description):
                parser = TestingFarmParser(testing_farm_content, self.args)
                self.assertEqual(expected, parser.get_tree_name())

    def test_parser_processed(self):
        """Identify when the parser have been used."""
        self.assertFalse(self.parser.processed)
        self.parser.process()
        self.assertTrue(self.parser.processed)

    def test_has_tests(self):
        """Ensure has_tests works."""
        # Change processed to get False
        self.parser.processed = True
        self.assertFalse(self.parser.has_tests())
        # The parser should process the file
        self.parser.processed = False
        self.assertTrue(self.parser.has_tests())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_warning_with_an_empty_test_suite_without_information(self, stderr_mock):
        """Test warning when the test suite is empty (TFT bug 2641)."""
        warning_message = (
            "Can't get arch for testsuite /plans/security/security-internal, "
            "skipping it"
        )
        tft_file = (ASSETS / 'testing_farm_provision_error.xml')
        testing_farm_content = pathlib.Path(tft_file).read_text(encoding='utf-8')
        self.parser = TestingFarmParser(testing_farm_content, self.args)
        self.parser.process()
        self.assertIn(warning_message, stderr_mock.getvalue())
