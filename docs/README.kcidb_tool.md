# kcidb_tool

## Getting started

Parser to transform test results (Beaker or Testing Farm) to kcidb format.

## Usage

Here are some main features:

### Create

Create a kcidb file from a result file, if task has a parameter with field `name` equals to
`MAINTAINERS` and the value can be a list of maintainers for the task. If this parameter is not
provided the default one provided in the CLI will be used.

The format to define maintainers is:

User name <user@emailaddress.com> / gitlab.com_username

Gitlab user is not mandatory and it needs the slash, if you want to multiples maintainers, you only
need to add a comma and define the next one, here is an example

User 1 <user_1@emailaddress.com> / user_1, User 2 <user_2@emailaddress.com> / user_2

Here is the subcommand help

```console
$ kcidb_tool create --help
usage: kcidb_tool create [-h] -c CHECKOUT --contact CONTACT --nvr NVR --source {beaker,testing-farm} [-a NAME=URL [NAME=URL ...]] [--brew-task-id BREW_TASK_ID] [-d] -i INPUT [-o OUTPUT] [--origin ORIGIN]
                         [--checkout-origin CHECKOUT_ORIGIN] [--builds-origin BUILDS_ORIGIN] [--tests-origin TESTS_ORIGIN] [-t TEST_PLAN] [--tests-provisioner-url TESTS_PROVISIONER_URL] [--src-nvr SRC_NVR]

Convert Beaker or TMT results file into kcidb format.

options:
  -h, --help            show this help message and exit
  -c CHECKOUT, --checkout CHECKOUT
                        The checkout name used to generate all the kcidb ids.
  --contact CONTACT     Contact email, "Full Name <username@domain>" and username@domain are accepted, this argument can be used several times.
  --nvr NVR             NVR info.
  --source {beaker,testing-farm}
                        Source of the original result file (beaker or testing-farm)..
  -a NAME=URL [NAME=URL ...], --add-output-files NAME=URL [NAME=URL ...]
                        Add a list of output files to every test, the syntax is name=url (example: job_url=https://jenkins/job/my_job/1)
  --brew-task-id BREW_TASK_ID
                        Id of the Brew task where the package was compiled.
  -d, --debug           Enable it if using kernel debug build. DEPRECATED, kcidb_tool will get the information from the nvr.
  -i INPUT, --input INPUT
                        Path to the original result file.
  -o OUTPUT, --output OUTPUT
                        Path to the KCIDB file (By default kcidb.json).
  --origin ORIGIN       The default origin for all objects (By default kcidb_tool). It can be overwritten with builds-origin, checkout-origin or tests-origin
  --checkout-origin CHECKOUT_ORIGIN
                        The origin for the checkout, if it's not provided will fallback to origin
  --builds-origin BUILDS_ORIGIN
                        The origin for all builds, if it's not provided will fallback to origin
  --tests-origin TESTS_ORIGIN
                        The origin for all tests, if it's not provided will fallback to origin
  -t TEST_PLAN, --test_plan TEST_PLAN
                        (DEPRECATED) Generate only test_plan, please use the subcommand create-test-plan.
  --tests-provisioner-url TESTS_PROVISIONER_URL
                        URL of the tests provisioner, usually jenkins job url.
  --src-nvr SRC_NVR     NVR for the source package, nvr value will be used if it is not provided
  --submitter SUBMITTER
                        Email of the person who submitted the job.
  --report-rules REPORT_RULES
                        An string with the rules to send reports, must be JSON blob and it's used by CKI
```

## Create Test Plan

Create a test plan from a kcidb file, here is the subcommand help

```console
$ kcidb_tool create-test-plan --help
usage: kcidb_tool create-test-plan [-h] --arch {x86_64,aarch64,ppc64le,s390x} -c CHECKOUT --nvr NVR [--brew-task-id BREW_TASK_ID] [-d] [-o OUTPUT] [--origin ORIGIN]
                                   [--checkout-origin CHECKOUT_ORIGIN] [--builds-origin BUILDS_ORIGIN] [--src-nvr SRC_NVR]

Create a simple test plan only with the system provision task.

options:
  -h, --help            show this help message and exit
  --arch {x86_64,aarch64,ppc64le,s390x}
                        The test plan's architecture.
  -c CHECKOUT, --checkout CHECKOUT
                        The checkout name used to generate all the kcidb ids.
  --nvr NVR             NVR info.
  --brew-task-id BREW_TASK_ID
                        Id of the Brew task where the package was compiled.
  -d, --debug           Enable it if using kernel debug build. DEPRECATED, kcidb_tool will get the information from the nvr.
  -o OUTPUT, --output OUTPUT
                        Path to the KCIDB file (By default kcidb.json).
  --origin ORIGIN       The default origin for all objects (By default kcidb_tool). It can be overwritten with builds-origin, checkout-origin or tests-origin
  --checkout-origin CHECKOUT_ORIGIN
                        The origin for the checkout, if it's not provided will fallback to origin
  --builds-origin BUILDS_ORIGIN
                        The origin for all builds, if it's not provided will fallback to origin
  --src-nvr SRC_NVR     NVR for the source package, nvr value will be used if it is not provided
```

### Merge

Merge multiple kcidb files, here is the subcommand help

```console
$ kcidb_tool merge --help
usage: kcidb_tool merge [-h] -r RESULT [-o OUTPUT]

Merge multiple kcidb files.

options:
  -h, --help            show this help message and exit
  -r RESULT, --result RESULT
                        Path to a source result (kcidb format).
  -o OUTPUT, --output OUTPUT
                        Path to the merged KCIDB file (By default merge_kcidb.json).
```

### Push2dw

Send a kcidb file to datawarehouse trough the API.

```console
$ kcidb_tool push2dw --help
usage: kcidb_tool push2dw [-h] --token TOKEN [-i INPUT] [--url URL]

Send kcidb results to the datawarehouse API.

optional arguments:
  -h, --help            show this help message and exit
  --token TOKEN         Token in Datawarehouse.
  -i INPUT, --input INPUT
                        The path to the kcidb file (By default kcidb.json).
  --url URL             Datawarehouse URL (By default https://datawarehouse.cki-project.org)
```

Token can be specified via `--token`, `DW_TOKEN` in the environment or
via a configuration file:

* `/etc/bkr2kcidb/config.ini`
* `~/.config/bkr2kcidb/config.ini`
* `/etc/kcidb_tool/config.ini`
* `~/.config/kcidb_too/config.ini`

```ini
[dw]
token = <your token>
```

## Push2UMB

Set a kcidb file to DataWarehouse via UMB.

```console
kcidb_tool push2umb --help
usage: kcidb_tool push2umb [-h] --certificate CERTIFICATE [-i INPUT]

Send kcidb results through UMB.

options:
  -h, --help            show this help message and exit
  --certificate CERTIFICATE
                        Certificate file (pem) to send messages through UMB. Can also set via UMB_CERTIFICATE environment variable.
  -i INPUT, --input INPUT
                        The path to the kcidb file (By default kcidb.json).
```

Basically a pem file contains the certificate and the key. For example, if you have `.crt` and
`.key` file, you can create the `.pem` file easilly with:

```console
cat file.crt file.key > file.pem
```
