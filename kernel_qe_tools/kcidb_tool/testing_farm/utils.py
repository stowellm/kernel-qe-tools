"""Test module."""
from itertools import chain
import xml.etree.ElementTree as ET

from .. import utils


def get_kcidb_test_status(testing_farm_result):
    """
    Return the KCIDB status of a test given a test case result from testing farm.

    According to the official documentation, test case could return those values:
    * undefined
    * error
    * passed
    * failed
    * info
    * not_applicable
    * skipped
    * needs_inspection (new status found at
    https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/-/issues/28)



    Valid KCIDB statuses for a tasks:

    * SKIP
    * DONE
    * PASS
    * MISS
    * ERROR
    * FAIL

    Source:
    Testing Farm:
    https://gitlab.com/testing-farm/gluetool-modules/-/blob/main/gluetool_modules_framework/libs/
    test_schedule.py?ref_type=heads#L148

    KCIDB:
    https://cki-project.org/docs/test-maintainers/status-meaning/
    """
    correlations = {
        'undefined': 'ERROR',
        'error': 'ERROR',
        'passed': 'PASS',
        'failed': 'FAIL',
        'info': 'PASS',
        'not_applicable': 'SKIP',
        'skipped': 'SKIP',
        'needs_inspection': 'ERROR'
    }
    return correlations[testing_farm_result]


def get_kcidb_result_status(check_result):
    """
    Return the status for a KCIDB result start from a check_result.

    We don't see any valid result for in a check, so this method is a dummy method

    Source:
    Testing Farm:
    https://gitlab.com/testing-farm/gluetool-modules/-/blob/main/gluetool_modules_framework/
    testing/test_schedule_tmt.py?ref_type=heads#L139

    KCIDB:
    https://cki-project.org/docs/test-maintainers/status-meaning/
    """
    return check_result.upper()


def get_arch_from_testsuite(testsuite):
    """Get the arch from a test suite."""
    value = None
    prop = testsuite.find(".testing-environment[@name='requested']/property[@name='arch']")
    try:
        value = prop.get('value')
    except AttributeError:
        pass
    return value


def get_hostname_from_testcase(testcase):
    """Get the hostname from a test suite."""
    value = None
    prop = testcase.find(".//property[@name='baseosci.connectable_host']")
    try:
        value = prop.get('value')
    except AttributeError:
        pass

    return value


def get_results(test, results, first_id):
    """Generate results for a test case."""
    all_results = []
    for number, result in enumerate(chain(results.iter('check'), results.iter('subresult')),
                                    first_id):
        name = result.get('name', 'UNDEFINED')
        all_results.append(utils.clean_dict({
            'id': f'{test["id"]}.{number}',
            'name': name,
            'comment': name,
            'status': get_kcidb_result_status(result.get('result')),
            'output_files': utils.get_output_files(result.find('logs')),
        }))

    return all_results


def create_virtual_result(test):
    """
    Generate a virtual result of every testcase.

    The virtual result will be the first always.

    There are some issues in TMT and in Testing Farm and we need to create it.

    * https://github.com/teemtee/tmt/issues/2826
    * https://issues.redhat.com/browse/TFT-2762
    """
    output_files = [output_file for output_file in test["output_files"]
                    if output_file['name'] == 'testout.log']
    return utils.clean_dict({
        'id': f'{test["id"]}.1',
        'name': test['path'],
        'comment': test['path'],
        'status': test['status'],
        'output_files': output_files
        })


def create_system_provision_test_case(test_suite, test_id):
    """
    Add a fake provision "testing farm" task to every testsuite.

    Testing farm does not offer to much information about the provision itself.
    There is an issue TFT 2641 that will provide more information.

    Right now, we're going to suppose that information is here.

    We have two scenarios.

    * The installation failed and we don't have any test case, but we have
    (or will have) information about the request. In that case, the status should
    be error

    * The installation and tests were executed, in that case the status is passed, because
    we don't have a status to define MISS in testing farm.
    """
    test_case = ET.fromstring('<testcase />')

    # Add basic information
    test_case.set('id', str(test_id))
    test_case.set('name', 'system_provision')

    # Add recipe logs
    test_case.append(test_suite.find('logs'))

    # Time -> Testing Farm does not provide information about the provisioning time
    test_case.set('time', '0')

    # Result
    # If we have any testcase, the status is passed, otherwise will be error
    if int(test_suite.get('tests', '0')) > 0:
        test_case.set('result', 'passed')
        check_case_result = 'pass'
    else:
        test_case.set("result", "error")
        check_case_result = "error"

    # Checks
    checks = ET.fromstring('<checks />')
    check = ET.fromstring('<check />')
    check.set('name', test_case.get('name'))
    check.set('result', check_case_result)
    check.append(test_suite.find('logs'))
    checks.append(check)
    test_case.append(checks)

    # Add extra information
    test_case.set('fake', 'True')  # This is a string not a boolean

    return test_case


def get_job_url(logs) -> str | None:
    """
    Get the job URL from the logs of a test suite.

    The job URL is the URL of the job in the CI/CD system.

    To get this information, we need to look for the log with the name 'workdir', and remove
    the last part of the path.

    Example:
      * Source -> https://${OSCI_URL}/testing-farm/1234/workdir/
      * Result -> https://${OSCI_URL}/testing-farm/1234
    """
    if logs is not None:
        for log in logs.iter('log'):
            if log.get("name", "UNDEFINED") == 'workdir':
                raw_url = log.get('href')
                if raw_url.endswith('/'):
                    return raw_url.rsplit('/', 2)[0]
                return raw_url.rsplit('/', 1)[0]
    return None


def remove_duplicated_logs(test):
    """Remove from root output files, logs that are already in a subtest."""
    for subtest in test['misc'].get('results', []):
        for log in subtest.get('output_files', []):
            # don't delete from the root if it is from the virtual subtest
            if log in test['output_files'] and log['name'] != 'testout.log':
                test['output_files'].remove(log)
