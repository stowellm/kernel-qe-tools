"""
Get Repository report.

This script will create a json file with all files categorized.

The report can be with all files in the repository, or using the changes
between two commits.

Categories:

* git
* shell_files
* tmt_directories
* markdown_files
* yaml_files
* shellspec_directories
* python_files
"""

import argparse
import json
import os
import pathlib
import sys
import typing

from cki_lib.logger import get_logger
from git import Repo
from git.exc import GitCommandError
from git.exc import InvalidGitRepositoryError

LOGGER = get_logger(__name__)


def get_git_changes(changes):
    """Gerenate git changes grouped by states."""
    result = {
        'modified': [],
        'copy-edit': [],
        'rename-edit': [],
        'added': [],
        'deleted': [],
        'unmerged': []

    }
    flags_states = {
        'M': 'modified',
        'C': 'copy-edit',
        'R': 'rename-edit',
        'A': 'added',
        'D': 'deleted',
        'U': 'unmerged'
    }

    if changes is not None:
        for (status, path) in changes:
            result[flags_states[status]].append(path)

    return result


def get_files_by_extensions(files, changes, extensions):
    """
    Generate a list of files by extensions.

    If changes is None, we'll get all files, otherwise only
    not deleted files from git will be added.
    """
    if changes is None:
        return [
            path for path in files if path.endswith(extensions)
        ]
    return [
        path for (flag, path) in changes if
        path.endswith(extensions) and flag != 'D'
    ]


def get_tmt_directories(files, changes):
    """
    Generate the list of tmt directories.

    To get all tmt directories we have store all directories which
    contains a fmf file. We need to exclude the root .fmf directory

    With changes, to get all related changes, we need to get all changes not unmerged.
    """
    tmt_directories = [
        os.path.dirname(file) for file in sorted(files, reverse=True) if
        file != '.fmf' and file.endswith('.fmf')
    ]

    if changes is None:
        _files = files
    else:
        _files = [path for (flag, path) in changes if flag != 'U']

    result = set()
    for file in _files:
        for tmt_directory in tmt_directories:
            if tmt_directory in file:
                result.add(tmt_directory)
                break

    return list(result)


def get_shellspec_directories(files, changes):
    """
    Generate the list of shellspec_directories directories.

    A shellpec directory contains a 'spec' folder and a '.shellspec' files

    With changes, to get all related changes, we need to get all changes not unmerged.
    """
    shellspec_directories = [
        os.path.dirname(file) for file in sorted(files, reverse=True) if
        file.endswith('.shellspec') and os.path.isdir(os.path.dirname(file) + '/spec')
    ]

    if changes is None:
        _files = files
    else:
        _files = [path for (flag, path) in changes if flag != 'U']

    result = set()
    for file in _files:
        for shellspec_directory in shellspec_directories:
            if shellspec_directory in file:
                result.add(shellspec_directory)
                break

    return list(result)


def get_report(files: str, changes: str) -> typing.Dict[typing.Any, typing.Any]:
    """Generate the report."""
    return {
        'data': {
            'type': 'only_mr_changes' if changes is not None else 'all_files',
            'git': get_git_changes(changes),
            'shell_files': get_files_by_extensions(files, changes, ('.sh', '.bash')),
            'tmt_directories': get_tmt_directories(files, changes),
            'markdown_files': get_files_by_extensions(files, changes, ('.md', '.MD')),
            'yaml_files': get_files_by_extensions(files, changes, ('.yml', '.yaml')),
            'shellspec_directories': get_shellspec_directories(files, changes),
            'python_files': get_files_by_extensions(files, changes, ('.py')),
        }
    }


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Command line interface to generate a json file with all files."""
    description = 'Generate a report to be used in other CI jobs.'

    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        '--first-commit',
        type=str,
        required=False,
        help='SHA of the last commit, ommit it to use all files.'
    )

    parser.add_argument(
        '--last-commit',
        type=str,
        required=False,
        help='SHA of the last commit, ommit it to use all files.'
    )

    parser.add_argument(
        '--output-file',
        type=str,
        required=False,
        default='ci_files.json',
        help='Output file (By default ci_files.json).'
    )

    parser.add_argument(
        '--working-dir',
        type=str,
        required=False,
        default='.',
        help='Git working directory (By default the current directory).'
    )

    # pylint: disable=duplicate-code
    parsed_args = parser.parse_args(args)
    if not pathlib.Path(parsed_args.working_dir).is_dir():
        LOGGER.error('%s is not a directory', parsed_args.working_dir)
        return 1
    try:
        repo = Repo(parsed_args.working_dir)
    except InvalidGitRepositoryError:
        LOGGER.error('%s does not contain a git repository', parsed_args.working_dir)
        return 1

    if parsed_args.first_commit and parsed_args.last_commit:
        LOGGER.info('Getting git changes')
        try:
            raw_changes = repo.git.diff(
                '--name-status',
                parsed_args.last_commit,
                parsed_args.first_commit)
            changes = []
            for line in raw_changes.split('\n'):
                fields = line.split('\t')
                # Store only two values, and use only the first character in the first field
                changes.append([fields[0][0], fields[1]])
        except GitCommandError:
            LOGGER.error('Unable to get git differences, could you check commit hashes.')
            return 1

    elif not parsed_args.first_commit and not parsed_args.last_commit:
        LOGGER.info('Generating the report with all repositories files')
        changes = None

    else:
        LOGGER.error('First and last commit must be provided together')
        return 1

    LOGGER.info('Getting all git files')
    files = repo.git.ls_files().split('\n')

    report = get_report(files, changes)

    LOGGER.info('Writing output file %s', parsed_args.output_file)

    output_file = pathlib.Path(parsed_args.output_file)
    output_file.parent.mkdir(parents=True, exist_ok=True)
    output_file.write_text(json.dumps(report, indent=4), encoding="utf-8")
    return 0


if __name__ == "__main__":  # pragma: no cover
    sys.exit(main(sys.argv[1:]))
